import { createStore } from "vuex";
import { apiFetchAllQuestions} from "./api/questions";
import {apiUserLoginRegister} from "./api/users"

const initUser = () => {
    const storedUser = localStorage.getItem("question-user")
    if(!storedUser) {
        return null
    }

    return JSON.parse(storedUser)
}

export default createStore ({
    state:{
        user:initUser(),
        questions : [],
        searchText: "",
        userFavourites: [],
        point : 0,
        selected:false
    },
    getters: {
        filterQuestionsBySearchText: (state) => {
            return state.questions.filter((question) => {
                return question.question.toLowerCase().includes(state.searchText.toLowerCase())
            })
        }
    },

    mutations: {
        setUser: (state, user) => {
            state.user = user
        },
        setQuestions : (state, questions) => {
            state.questions = questions
        },
        setSearchText: (state, text) => {
            state.searchText = text
        }, 
        setPoint:(state,point)=>{
            state.point=point
        },
        setSelected:(state,selected)=>{
            state.selected=selected
        }

        // setUserFavourites: (state, favourites) => {
        //     state.userFavourites = favourites
        // },
        // addToUserFavourites: (state,movieId) => {
        //     const movie = state.movies.find(movie => movie.id === movieId)
        //     state.userFavourites.push(movie)
        // }, 
        // setMovieAsFavourite: (state, movieIdToFavourite) => {
        //     const movie = state.movies.find(movie => movie.id === movieIdToFavourite)
        //     if(!movie) return
        //     movie.isFavourite = true
        // }   

    },
    actions:{
        async changePoint({commit},){
            console.log("in store .js")
            commit("setPoint",this.state.point+1);
        },
        async changeSelected({commit},){
            
            commit("setSelected",!this.state.selected)
            console.log("in store .js2 "+this.state.selected)
        }
        ,
        async loginRegister({commit}, {username, action}){
            try {
                if(action !=="login" && action !== "register"){
                    throw new Error("loginRegister: Unknown action provided " + action)
                }
                const [error, user] = await apiUserLoginRegister(
                    action,
                    username.value,
                    )

                    if(error !== null) {
                        throw new Error(error)
                    }

                    commit("setUser", user)
                    localStorage.setItem("question-user", JSON.stringify(user))

                    return null
                }
            catch(e){
                return e.message
            }

            },
        async fetchAllQuestions({ commit }) {
                const [error, questions] = await apiFetchAllQuestions()
                if(error !== null){
                    return error
                }


                commit("setQuestions", questions)
                return null //error
        }
        // async fetchUserFavourites({commit,state}) {
        //     const [error, movies] = await apiFetchUserFavouriteMovies(state.user.id)
        //     if(error !== null){
        //         return error
        //     }
        //     for(const movie of movies){
        //         commit("setMovieAsFavourite",movie.id)
        //     }

        //     commit("setUserFavourites", movies)
        //     return null //error
        // },

        // async addMovieToFavourites({commit, state},movieId) {
        //     const [error] = await apiAddMovieToUserFavourites(movieId, state.user.id)
        //     if(error !== null){
        //         return error
        //     }

        //     commit("addToUserFavourites", movieId)
        //     return null //error
        // }
    },
})