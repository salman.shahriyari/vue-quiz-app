# QuizApp

Trivia Quiz is a game app written by using vue as the main front-end language. The questions come from different categories with different difficulty level.


## Getting Started

### Dependencies

* First need to install npm: npm install
* to run: npm run dev

### Executing program

* Download the program code and run it in your local machine. 
* Then you are free to make your required changes.

## Usage

* At first, your username is asked and then you can start playing the game.

* There are 10 questions that you need to answer 

* after answering the questions, click the submit button to see the answer


## Contributing
It is a demo app and you are welcome to give your opinions to make the app features and its visualisation better


## Authors

* Salman Shahriyari  
* [@salman3sh]

## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the [MIT] License - see the LICENSE.md file for details

## Acknowledgments and inspiration

* Dewald Els
* Noroff.no
