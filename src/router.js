import { createRouter, createWebHistory } from "vue-router"
// import store from "./store"
import Login from "./views/Login.vue"
import Question from "./views/Questions.vue"
import Profile from "./views/profile.vue"


// const authGuard = (to, from,next) => {
//     if(!store.state.user) {
//         next("/")
//     }else {
//         next()
//     }
// }

// const loginGuard = (_to,_from, next) => {
//     if(store.state.user) {
//         next("/movies")
//     }
//     else {
//         next()
//     }
// }

const routes = [
    {
        path:"/",
        component: Login,
        // beforeEnter: loginGuard 

    },
    {
        path:"/questions",
        component: Question,
        // beforeEnter: authGuard
    },
    {
        path:"/profile",
        component: Profile,
        // beforeEnter: authGuard
    },

]

export default createRouter({
    history: createWebHistory(),
    routes
})