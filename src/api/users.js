// import { BASE_URL } from "./";

export async function apiUserLoginRegister(action="login", username) {
    try {
        const config = {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                user: {
                    username
                }
            })
        }
        const response = await fetch(`https://noroff-salman.herokuapp.com/trivia/${action}`, config)
        const {success, data, error="An error occured"} = await response.json()
        if(!success) {
            throw new Error(error)
        }
        return [null, data] 
    }
    catch(error) {
        return [error.message, null]

    }
}