import  {BASE_URL} from "."

export async function apiFetchAllQuestions() {
    try {
        const response = await fetch (`${BASE_URL}`)

        if(!response.ok) {
            throw new Error ("could not find questions")
        }

        const {response_code, results, error="Could not fetch questions"} = await response.json()

        // if(!response_code) {
        //     throw new Error(error)
        // }

        return [null, results]
    }
    catch(e) {
        return [e.message, [] ]
    }
}

// export async function apiFetchUserFavouriteMovies(userId) {
//     try {
//         const response = await fetch (`${BASE_URL}/users/${userId}/favourites`)

//         if(!response.ok) {
//             throw new Error ("could not find favourite movies")
//         }

//         const {success, data, error="Could not fetch favourite movies"} = await response.json()

//         if(!success) {
//             throw new Error(error)
//         }

//         return [null, data]
//     }
//     catch(e) {
//         return [e.message, [] ]
//     }
// }

// export async function apiAddMovieToUserFavourites(movieId, userId) {
//     try {
//         const response = await fetch (`${BASE_URL}/users/favourites`,{
//             method: "POST",
//             headers: {
//                 "content-type": "application/json"
//             },
//             body: JSON.stringify({
//                 userId,
//                 movieId

//             })
//         })

//         if(!response.ok) {
//             throw new Error ("could not add movie to favourite ")
//         }

//         const {success, data, error="could not add movie to favourite"} = await response.json()

//         if(!success) {
//             throw new Error(error)
//         }

//         return [null, data]
//     }
//     catch(e) {
//         return [e.message, [] ]
//     }
// }